﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD_Classes_Visualizer.Data
{
    public class ClassesOfUseCases
    {
        public string IdUseCaseEntry { get; set; }
        public string PathUseCase { get; set; }
        public string IdUseCase { get; set; }
        public string NameUseCase { get; set; }
        public string IdClass { get; set; }
        public string NameClass { get; set; }
    }
}
