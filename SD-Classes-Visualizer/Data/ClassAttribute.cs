﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD_Classes_Visualizer.Data
{
    public class ClassAttribute
    {
        public string IdClass { get; set; }
        public string NameClass { get; set; }
        public string IdClassAttribute { get; set; }
        public string NameClassAttribute { get; set; }
        public long AttributeOrder { get; set; }
        public string IdAttributeType { get; set; }
        public string NameAttributeType { get; set; }
        public string IdReferenceClass { get; set; }
        public string NameReferenceClass { get; set; }
        public string IdOAttIsList { get; set; }
        public bool IsList { get; set; }

        public ClassAttribute()
        {

        }
        public ClassAttribute(clsObjectRel classToAttribute,
            clsObjectRel attributeToType,
            clsObjectRel attributeTypeToClass,
            clsObjectAtt listAttributes)
        {
            IdClass = classToAttribute.ID_Object;
            NameClass = classToAttribute.Name_Object;
            IdClassAttribute = classToAttribute.ID_Other;
            NameClassAttribute = classToAttribute.Name_Other;
            AttributeOrder = classToAttribute.OrderID != null ? classToAttribute.OrderID.Value : 0;
            IdAttributeType = attributeToType.ID_Other;
            NameAttributeType = attributeToType.Name_Other;
            IdReferenceClass = attributeTypeToClass != null ? attributeTypeToClass.ID_Other : null;
            NameReferenceClass = attributeTypeToClass != null ? attributeTypeToClass.Name_Other : null;
            IdOAttIsList = listAttributes != null ? listAttributes.ID_Attribute : null;
            IsList = listAttributes != null ? listAttributes.Val_Bit == null ? false : (bool) listAttributes.Val_Bit : false;

            NameClassAttribute += " : " + (!string.IsNullOrEmpty(NameReferenceClass) ? NameReferenceClass : NameAttributeType) + (IsList ? "[]" : "");

        }
    }
}
