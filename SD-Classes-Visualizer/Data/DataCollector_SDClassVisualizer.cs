﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UseCase_Visualizer;

namespace SD_Classes_Visualizer.Data
{
    public class DataCollector_SDClassVisualizer
    {
        private clsLocalConfig localConfig;
        private clsDataWork_USECase dataWork_useCase;
        private OntologyModDBConnector dbConnector_ClassesOfUseCases;
        private OntologyModDBConnector dbConnector_AttributesOfClasses;
        private OntologyModDBConnector dbConnector_ListAttributes;
        private OntologyModDBConnector dbConnector_TypeOfAttribute;
        private OntologyModDBConnector dbConnector_TypeOfAttributeToClass;

        public List<clsObjectRel> ProjectHierarchy
        {
            get { return dataWork_useCase.ProjectHierarchy; }
        }

        public List<clsObjectRel> ProjectResourcesOfProjects
        {
            get { return dataWork_useCase.ProjectResourcesOfProjects; }
        }

        public List<clsObjectRel> UseCasesOfProjectResources
        {
            get { return dataWork_useCase.UseCasesOfProjectResources; }
        }

        public List<clsOntologyItem> SoftwareProjects 
        {
            get { return dataWork_useCase.SoftwareProjects; }
            set { dataWork_useCase.SoftwareProjects = value; }
        }

        public List<clsObjectRel> UseCasesOfSoftwareProjects 
        {
            get { return dataWork_useCase.UseCasesOfSoftwareProjects; }
            set { dataWork_useCase.UseCasesOfSoftwareProjects = value; }
        }

        public List<clsUseCase> UseCaseList 
        {
            get { return dataWork_useCase.UseCaseList; }
            set { dataWork_useCase.UseCaseList = value; }
        }
        public List<clsObjectRel> UseCaseToUseCase 
        {
            get { return dataWork_useCase.UseCaseToUseCase; }
            set { dataWork_useCase.UseCaseToUseCase = value; }
        }

        public List<ProjectToSoftwareProject> SoftwareProjectToProject
        {
            get { return dataWork_useCase.SoftwareProjectToProject; }
            set { dataWork_useCase.SoftwareProjectToProject = value; }
        }

        public clsOntologyItem GraphML_UML_ActorSource
        {
            get { return dataWork_useCase.GraphML_UML_ActorSource; }
        }

        public List<ClassesOfUseCases> ClassesOfUseCases
        {
            get;
            set;
        }

        public List<ClassAttribute> ClassAttributes
        {
            get;
            set;
        }

        public clsOntologyItem GetDataUseCases()
        {
            var result = dataWork_useCase.GetData_SoftwareProjects();
            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                result = dataWork_useCase.GetData_ProjectOfUseCases(); 
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    result = GetDataClasses();
                }
            }

            return result;
        }

        private clsOntologyItem GetDataClasses()
        {
            var result = localConfig.Globals.LState_Success.Clone();

            var searchClasses = UseCaseList.Select(uc => new clsObjectRel
                {
                    ID_Object = uc.IdUseCase,
                    ID_RelationType = localConfig.OItem_relationtype_needs.GUID,
                    ID_Parent_Other = localConfig.OItem_class_classes.GUID
                }).ToList();

            if (searchClasses.Any())
            {
                result = dbConnector_ClassesOfUseCases.GetDataObjectRel(searchClasses);

                
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    
                    ClassesOfUseCases = dbConnector_ClassesOfUseCases.ObjectRels.Select(cls => new ClassesOfUseCases
                        {
                            IdUseCaseEntry = cls.ID_Object,
                            PathUseCase = cls.ID_Object + @"\",
                            IdClass = cls.ID_Other,
                            NameClass = cls.Name_Other,
                            IdUseCase = cls.ID_Object,
                            NameUseCase = cls.Name_Object
                        }).ToList();

                    result = GetDataAttributes();

                    if (result.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        result = GetDataAttributeTypes();
                        if (result.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            ClassAttributes = (from attrib in dbConnector_AttributesOfClasses.ObjectRels
                                              join attType in dbConnector_TypeOfAttribute.ObjectRels on attrib.ID_Other equals attType.ID_Object
                                              join classOfAttType in dbConnector_TypeOfAttributeToClass.ObjectRels on attType.ID_Other equals classOfAttType.ID_Object into classOfAttTypes
                                              from classOfAttType in classOfAttTypes.DefaultIfEmpty()
                                              join listAttribute in dbConnector_ListAttributes.ObjAtts on attType.ID_Object equals listAttribute.ID_Object into listAttributes
                                               from listAttribute in listAttributes.DefaultIfEmpty()
                                              select new ClassAttribute(attrib, attType,classOfAttType, listAttribute)).ToList();

                            ConfigureUseCaseToClasses();
                        }
                        
                    }
                    

                }
            }
            

            return result;

        }

        private clsOntologyItem GetDataAttributes()
        {
            var result = localConfig.Globals.LState_Success.Clone();
            var searchAttributes = ClassesOfUseCases.Select(clsuc => new clsObjectRel
                {
                    ID_Object = clsuc.IdClass,
                    ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                    ID_Parent_Other = localConfig.OItem_class_classattribute.GUID
                }).ToList();

            if (searchAttributes.Any())
            {
                result = dbConnector_AttributesOfClasses.GetDataObjectRel(searchAttributes);
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var searchListAttributes = dbConnector_AttributesOfClasses.ObjectRels.Select(att => new clsObjectAtt
                        {
                            ID_Object = att.ID_Other,
                            ID_AttributeType = localConfig.OItem_attributetype_is_list.GUID
                        }).ToList();

                    if (searchListAttributes.Any())
                    {
                        result = dbConnector_ListAttributes.GetDataObjectAtt(searchListAttributes);
                    }
                }
            }

            return result;
        }

        private void ConfigureUseCaseToClasses()
        {
            var parentUseCases = (from useCaseClass in ClassesOfUseCases
                                  join useCaseToUseCase in dataWork_useCase.UseCaseToUseCase on useCaseClass.IdUseCaseEntry equals useCaseToUseCase.ID_Other
                                  select new { useCaseClass, useCaseToUseCase }).ToList();

            if (parentUseCases.Any())
            {
                parentUseCases.ForEach(puc =>
                    {
                        puc.useCaseClass.IdUseCaseEntry = puc.useCaseToUseCase.ID_Object;
                        puc.useCaseClass.PathUseCase = @"\" + puc.useCaseToUseCase.ID_Object + (puc.useCaseClass.PathUseCase.StartsWith(@"\") ? "": @"\") + puc.useCaseClass.PathUseCase;
                    });

                ConfigureUseCaseToClasses();
            }

            
        }

        private clsOntologyItem GetDataAttributeTypes()
        {
            var result = localConfig.Globals.LState_Success.Clone();
            var searchAttributeTypes = dbConnector_AttributesOfClasses.ObjectRels.Select(att => new clsObjectRel
                {
                    ID_Object = att.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_is_of_type.GUID,
                    ID_Parent_Other = localConfig.OItem_class_classattribute_type.GUID
                }).ToList();

            if (searchAttributeTypes.Any())
            {
                result = dbConnector_TypeOfAttribute.GetDataObjectRel(searchAttributeTypes);

                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    var searchAttributeTypeToClass = dbConnector_TypeOfAttribute.ObjectRels.Select(attt => new clsObjectRel
                        {
                            ID_Object = attt.ID_Other,
                            ID_RelationType = localConfig.OItem_relationtype_ist.GUID,
                            ID_Parent_Other = localConfig.OItem_class_classes.GUID
                        }).ToList();

                    if (searchAttributeTypeToClass.Any())
                    {
                        result = dbConnector_TypeOfAttributeToClass.GetDataObjectRel(searchAttributeTypeToClass);
                    }
                }
            }

            return result;
        }

        public DataCollector_SDClassVisualizer(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            dataWork_useCase = new clsDataWork_USECase(localConfig.Globals);
            dbConnector_ClassesOfUseCases = new OntologyModDBConnector(localConfig.Globals);
            dbConnector_AttributesOfClasses = new OntologyModDBConnector(localConfig.Globals);
            dbConnector_ListAttributes = new OntologyModDBConnector(localConfig.Globals);
            dbConnector_TypeOfAttributeToClass = new OntologyModDBConnector(localConfig.Globals);
            dbConnector_TypeOfAttribute = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
