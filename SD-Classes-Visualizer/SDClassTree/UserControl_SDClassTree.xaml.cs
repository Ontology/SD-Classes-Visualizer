﻿using OntologyAppDBConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SD_Classes_Visualizer.SDClassTree
{
    /// <summary>
    /// Interaction logic for UserControl_SDClassTree.xaml
    /// </summary>
    public partial class UserControl_SDClassTree : UserControl
    {
        private ViewModel_SDClassTree viewModel;
        private Point _lastMouseDown;
        SDClassTreeNode draggedItem, _target;

        public static readonly DependencyProperty GlobalsProperty = DependencyProperty.Register
            (
                 "Globals",
                 typeof(Globals),
                 typeof(UserControl_SDClassTree),
                 new PropertyMetadata(null)
            );

        private Globals globals;
        public Globals Globals
        {
            get { return globals; }
            set
            {
                globals = value;
                viewModel = (ViewModel_SDClassTree)DataContext;
                viewModel.Globals = globals;
            }
        }

        private void TreeView_OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            var node = (SDClassTreeNode)e.NewValue;
            viewModel.CurrSelItem = node;
            //viewModel.GetFilterItemsByTreeNode(node);
            //viewModel.SetDataGridViewColumnAttributes();

        }

        private void treeView_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            try
            {
                //if (e.LeftButton == MouseButtonState.Pressed)
                //{
                //    Point currentPosition = e.GetPosition(tvFilter);

                //    if ((Math.Abs(currentPosition.X - _lastMouseDown.X) > 10.0) ||
                //        (Math.Abs(currentPosition.Y - _lastMouseDown.Y) > 10.0))
                //    {
                //        draggedItem = (OTreeNode)tvFilter.SelectedItem;
                //        if (draggedItem != null)
                //        {
                //            System.Windows.DragDropEffects finalDropEffect =
                //DragDrop.DoDragDrop(tvFilter,
                //    tvFilter.SelectedValue,
                //                System.Windows.DragDropEffects.Move);
                //            //Checking target is not null and item is 
                //            //dragging(moving)
                //            if ((finalDropEffect == System.Windows.DragDropEffects.Move) &&
                //    (_target != null))
                //            {
                //                // A Move drop was accepted
                //                if (draggedItem.Id != _target.Id)
                //                {
                //                    CopyItem(draggedItem, _target);
                //                    _target = null;
                //                    draggedItem = null;
                //                }
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception)
            {
            }
        }

        private void treeView_DragOver(object sender, System.Windows.DragEventArgs e)
        {
            try
            {
                //Point currentPosition = e.GetPosition(tvFilter);

                //if ((Math.Abs(currentPosition.X - _lastMouseDown.X) > 10.0) ||
                //   (Math.Abs(currentPosition.Y - _lastMouseDown.Y) > 10.0))
                //{
                //    // Verify that this is a valid drop and then store the drop target
                //    ContentPresenter cp = (ContentPresenter)e.Source;
                //    if (cp.Content is OTreeNode)
                //    {
                //        OTreeNode item = (OTreeNode)cp.Content;
                //        if (CheckDropTarget(draggedItem, item))
                //        {
                //            e.Effects = System.Windows.DragDropEffects.Move;
                //        }
                //        else
                //        {
                //            e.Effects = System.Windows.DragDropEffects.None;
                //        }
                //    }

                //}
                e.Handled = true;
            }
            catch (Exception)
            {
            }
        }

        private void treeView_Drop(object sender, System.Windows.DragEventArgs e)
        {
            try
            {
                //e.Effects = System.Windows.DragDropEffects.None;
                //e.Handled = true;

                //// Verify that this is a valid drop and then store the drop target
                //ContentPresenter cp = (ContentPresenter)e.Source;
                //if (cp.Content is OTreeNode)
                //{
                //    OTreeNode TargetItem = (OTreeNode)cp.Content;
                //    if (TargetItem != null && draggedItem != null)
                //    {
                //        _target = TargetItem;
                //        e.Effects = System.Windows.DragDropEffects.Move;
                //    }
                //}

            }
            catch (Exception)
            {
            }
        }

        private bool CheckDropTarget(SDClassTreeNode _sourceItem, SDClassTreeNode _targetItem)
        {
            //Check whether the target item is meeting your condition
            bool _isEqual = false;
            if (_sourceItem.IdNode != _targetItem.IdNode)
            {
                _isEqual = true;
            }
            return _isEqual;

        }

        private void CopyItem(SDClassTreeNode _sourceItem, SDClassTreeNode _targetItem)
        {

            //Asking user wether he want to drop the dragged TreeViewItem here or not
            //if (System.Windows.MessageBox.Show("Would you like to drop " + _sourceItem.Name + " into " + _targetItem.Name + "", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            //{
            //    try
            //    {
            //        //finding Parent TreeViewItem of dragged TreeViewItem 
            //        if (_sourceItem.ParentNode != null)
            //        {
            //            _sourceItem.ParentNode.Nodes.Remove(_sourceItem);
            //        }

            //        //adding dragged TreeViewItem in target TreeViewItem
            //        addChild(_sourceItem, _targetItem);



            //    }
            //    catch
            //    {

            //    }
            //}

        }

        public void addChild(SDClassTreeNode _sourceItem, SDClassTreeNode _targetItem)
        {
            // add item in target TreeViewItem 
            //_targetItem.Nodes.Add(_sourceItem);
            //_sourceItem.ParentNode = _targetItem;
            //viewModel.RefreshTreeView();
        }

        private TreeViewItem GetNearestContainer(UIElement element)
        {
            // Walk up the element tree to the nearest tree view item.
            TreeViewItem container = element as TreeViewItem;
            while ((container == null) && (element != null))
            {
                element = VisualTreeHelper.GetParent(element) as UIElement;
                container = element as TreeViewItem;
            }
            return container;
        }

        private void treeView_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                _lastMouseDown = e.GetPosition(tvFilter);
            }
        }

        private void CreateCompleteGraph_Click(object sender, RoutedEventArgs e)
        {
            viewModel.CreateCompleteGraph_Click();
        }

        private void CreateClassGraph_Click(object sender, RoutedEventArgs e)
        {
            viewModel.CreateClassGraph_Click();
        }

        public UserControl_SDClassTree()
        {
            InitializeComponent();
        }
    }
}
