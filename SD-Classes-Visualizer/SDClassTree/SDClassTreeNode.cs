﻿using SD_Classes_Visualizer.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;

namespace SD_Classes_Visualizer.SDClassTree
{
    [Flags]
    public enum NodeType
    {
        Root = 0,
        Project = 1,
        SoftwareProject = 2,
        UseCase = 3,
        Class = 4
    }
    public class SDClassTreeNode : NotifyPropertyChange
    {
        private string idNode;
        public string IdNode 
        {
            get { return idNode; }
            set
            {
                idNode = value;
                RaisePropertyChanged("IdNode");
            }
        }

        private string nameNode;
        public string NameNode
        {
            get { return nameNode; }
            set
            {
                nameNode = value;
                RaisePropertyChanged("NameNode");
            }
        }

        public ImageSource SourceOfImage
        {
            get { return nodeImage.Source; }
        }

        private Image nodeImage;
        public Image NodeImage
        {
            get { return nodeImage; }
            set
            {
                nodeImage = value;
                RaisePropertyChanged("NodeImage");
                RaisePropertyChanged("SourceOfImage");
            }
        }

        private NodeType nodeType;
        public NodeType NodeType
        {
            get { return nodeType; }
            set
            {
                nodeType = value;
                RaisePropertyChanged("NodeType");
            }
        }

        private List<SDClassTreeNode> nodes;
        public List<SDClassTreeNode> Nodes
        {
            get { return nodes; }
            set
            {
                nodes = value;
                RaisePropertyChanged("Nodes");
            }
        }

        private string nodePath;
        public string NodePath
        {
            get { return nodePath; }
            set
            {
                nodePath = value;
                RaisePropertyChanged("NodePath");
            }
        }

        public SDClassTreeNode()
        {
            Nodes = new List<SDClassTreeNode>();
        }
        


    }
}
