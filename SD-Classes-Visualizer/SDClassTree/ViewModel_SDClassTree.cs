﻿using GraphMLConnector;
using Microsoft.Win32;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using SD_Classes_Visualizer.BaseClasses;
using SD_Classes_Visualizer.Data;
using SD_Classes_Visualizer.Factory;
using SD_Classes_Visualizer.ViewModelUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using UseCase_Visualizer;

namespace SD_Classes_Visualizer.SDClassTree
{
    public class ViewModel_SDClassTree : ViewModelBase
    {
        private DataCollector_SDClassVisualizer dataCollector_SDClassVisualizer;
        private clsNodeEdgeFactory useCaseNodeEdgeFactory;
        private GraphMLItemFactory graphMLItemFactory;
        private clsGraphMLWork graphMLWork;

        private NodeFactory nodeFactory;
        private clsLocalConfig localConfig;
        private Globals globals;

        private string caption_CreateCompleteGraph;
        public string Caption_CreateCompleteGraph
        {
            get
            {
                return caption_CreateCompleteGraph;
            }
            set
            {
                caption_CreateCompleteGraph = value;
                RaisePropertyChanged("Caption_CreateCompleteGraph");
            }
        }

        private string caption_CreateClassGraph;
        public string Caption_CreateClassGraph
        {
            get
            {
                return caption_CreateClassGraph;
            }
            set
            {
                caption_CreateClassGraph = value;
                RaisePropertyChanged("Caption_CreateClassGraph");
            }
        }

        public RelayCommand<EventArgsSelectSDClassTreeNode> MySelItemChgCmd
        {
            get;
            private set;
        }


        private bool createCompleteGraphEnable;
        public bool CreateCompleteGraphEnable
        {
            get
            {
                createCompleteGraphEnable = false;
                if (currSelItem != null && currSelItem.NodeType == NodeType.Project)
                {
                    createCompleteGraphEnable = true;
                }

                return createCompleteGraphEnable;
            }
        }

        private bool createClassGraphEnable;
        public bool CreateClassGraphEnable
        {
            get
            {
                createClassGraphEnable = false;
                if (currSelItem != null && currSelItem.NodeType == NodeType.Project)
                {
                    createClassGraphEnable = true;
                }

                return createClassGraphEnable;
            }
        }

        public Globals Globals
        {
            get { return globals; }
            set
            {
                globals = value;


                localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
                if (localConfig == null)
                {
                    localConfig = new clsLocalConfig(new Globals());
                    LocalConfigManager.AddLocalConfig(localConfig);
                }

                dataCollector_SDClassVisualizer = new DataCollector_SDClassVisualizer(localConfig);
                graphMLWork = new clsGraphMLWork(localConfig.Globals);
                graphMLItemFactory = new GraphMLItemFactory(localConfig);
                useCaseNodeEdgeFactory = new clsNodeEdgeFactory(localConfig.Globals);
                nodeFactory = new NodeFactory(localConfig);

                var result = dataCollector_SDClassVisualizer.GetDataUseCases();
                if (result.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    Nodes = nodeFactory.GetNodes(dataCollector_SDClassVisualizer.ProjectHierarchy, 
                        dataCollector_SDClassVisualizer.ProjectResourcesOfProjects, 
                        dataCollector_SDClassVisualizer.SoftwareProjectToProject,
                        dataCollector_SDClassVisualizer.UseCasesOfSoftwareProjects,
                        dataCollector_SDClassVisualizer.ClassesOfUseCases);

                    RaisePropertyChanged("Nodes");
                }
                SetCaption();
                //Nodes.Add(new OTreeNode(localConfig));
                //dataCollector_ColumnFilter = new DataCollector_ColumnFilter(localConfig, gridItem);

                
            }
        }

        private void SetCaption()
        {
            Caption_CreateCompleteGraph = "Create Use-Case- + Class-Graph...";
            Caption_CreateClassGraph = "Create Class-Graph...";

        }

        private SDClassTreeNode currSelItem;
        public SDClassTreeNode CurrSelItem
        {
            get { return currSelItem; }
            set
            {
                currSelItem = value;
                RaisePropertyChanged("CurrSelItem");
                RaisePropertyChanged("CreateCompleteGraphEnable");
            }
        }

        private List<SDClassTreeNode> nodes;
        public List<SDClassTreeNode> Nodes
        {
            get { return nodes; }
            set
            {
                nodes = value;
                RaisePropertyChanged("Nodes");
            }
        }

        public ViewModel_SDClassTree()
        {
            MySelItemChgCmd = new RelayCommand<EventArgsSelectSDClassTreeNode>(Select_Item);
            
        }


        public void CreateCompleteGraph_Click()
        {
            if (currSelItem != null)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.DefaultExt = ".graphml";
                saveFileDialog.Filter = "GraphML Files (.graphml)|*.graphml";

                Nullable<bool> result = saveFileDialog.ShowDialog();

                if (result == true)
                {
                    var fileName = saveFileDialog.FileName;

                    var oResult = useCaseNodeEdgeFactory.CreateLists(dataCollector_SDClassVisualizer.UseCaseToUseCase, dataCollector_SDClassVisualizer.UseCaseList);

                    if (oResult.GUID == localConfig.Globals.LState_Success.GUID)
                    {
                        oResult = graphMLItemFactory.CreateLists(GraphType.ClassesAndUseCases, dataCollector_SDClassVisualizer.ClassesOfUseCases, 
                            useCaseNodeEdgeFactory.Nodes,
                            dataCollector_SDClassVisualizer.ClassAttributes);
                        if (oResult.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            var nodes = useCaseNodeEdgeFactory.Nodes;
                            var edges = useCaseNodeEdgeFactory.Edges;
                            var groupItems = useCaseNodeEdgeFactory.GroupItems;

                            nodes.AddRange(graphMLItemFactory.Nodes);
                            edges.AddRange(graphMLItemFactory.Edges);
                            graphMLWork.ExportGraph(fileName, nodes, edges, groupItems, new List<clsOntologyItem>{dataCollector_SDClassVisualizer.GraphML_UML_ActorSource});
                        }
                        else
                        {
                            MessageBox.Show("Die Klasseninformationen konnte nicht geladen werden!", "Fehler!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                        
                    }
                    else
                    {
                        MessageBox.Show("Die Use Case Information konnten nicht geladen werden!", "Fehler!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                    
                }
            }
        }

        public void CreateClassGraph_Click()
        {
            if (currSelItem != null)
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.DefaultExt = ".graphml";
                saveFileDialog.Filter = "GraphML Files (.graphml)|*.graphml";

                Nullable<bool> result = saveFileDialog.ShowDialog();

                if (result == true)
                {
                    var fileName = saveFileDialog.FileName;
                    var oResult = useCaseNodeEdgeFactory.CreateLists(dataCollector_SDClassVisualizer.UseCaseToUseCase, dataCollector_SDClassVisualizer.UseCaseList);

                    if (oResult.GUID == localConfig.Globals.LState_Success.GUID)
                    {


                        oResult = graphMLItemFactory.CreateLists(GraphType.Classes, dataCollector_SDClassVisualizer.ClassesOfUseCases,
                            useCaseNodeEdgeFactory.Nodes,
                            dataCollector_SDClassVisualizer.ClassAttributes);
                        if (oResult.GUID == localConfig.Globals.LState_Success.GUID)
                        {
                            var nodes = graphMLItemFactory.Nodes;
                            var edges = graphMLItemFactory.Edges;
                            var groupItems = graphMLItemFactory.GetGroupItemsOfNodes(useCaseNodeEdgeFactory.GroupItems, dataCollector_SDClassVisualizer.ClassesOfUseCases, nodes);

                            graphMLWork.ExportGraph(fileName, nodes, edges, groupItems, new List<clsOntologyItem> { dataCollector_SDClassVisualizer.GraphML_UML_ActorSource });
                        }
                        else
                        {
                            MessageBox.Show("Die Klasseninformationen konnte nicht geladen werden!", "Fehler!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Die Use Case Information konnten nicht geladen werden!", "Fehler!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    }
                    

                }
            }
        }

        

        private void Select_Item(EventArgsSelectSDClassTreeNode obj)
        {
            
        }

        
    }
}
