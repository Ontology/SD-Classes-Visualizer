﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD_Classes_Visualizer.SDClassTree
{
    public class EventArgsSelectSDClassTreeNode : EventArgs
    {
        public SDClassTreeNode ClassTreeNode { get; set;}
    }
}
