﻿using GraphMLConnector;
using OntologyClasses.BaseClasses;
using SD_Classes_Visualizer.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SD_Classes_Visualizer.Factory
{
    [Flags]
    public enum GraphType
    {
        None = 0,
        ClassesAndUseCases = 1,
        Classes = 2
    }
    public class GraphMLItemFactory
    {

        public List<clsGroupItem> GroupItems { get; private set; }
        public List<clsNodeItem> Nodes { get; private set; }
        public List<clsEdgeItem> Edges { get; private set; }
        private clsLocalConfig localConfig;

        public GraphMLItemFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            Nodes = new List<clsNodeItem>();
            Edges = new List<clsEdgeItem>();
            GroupItems = new List<clsGroupItem>();
        }

        public clsOntologyItem CreateLists(GraphType graphType, List<ClassesOfUseCases> classesOfUseCasesList, 
            List<clsNodeItem> useCaseNodes, 
            List<ClassAttribute> classAttributes)
        {
            
            var classNodes = classesOfUseCasesList.GroupBy(clsuc => new { IDClass = clsuc.IdClass, NameClass = clsuc.NameClass }).Select(cls => new clsNodeItem
            {
                IdNode = cls.Key.IDClass,
                NameNode = cls.Key.NameClass,
                XmlTemplate = localConfig.OItem_object_graphml___uml_class_node_with_methods
            }).ToList();

            classNodes.ForEach(cls =>
            {
                var attribList = new clsSubNodeList
                {
                    Variable = localConfig.OItem_object_attrib_list.Name
                };

                var methodList = new clsSubNodeList
                {
                    Variable = localConfig.OItem_object_method_list.Name
                };

                attribList.SubNodes = classAttributes.Where(att => att.IdClass == cls.IdNode).OrderBy(att => att.AttributeOrder).ThenBy(att => att.NameClassAttribute).Select(att => new clsNodeItem
                {
                    IdNode = att.IdClassAttribute,
                    NameNode = att.NameClassAttribute
                }).ToList();

                cls.SubNodeList.Add(attribList);
                cls.SubNodeList.Add(methodList);

            });

            Nodes.AddRange(classNodes);
            if (graphType == GraphType.ClassesAndUseCases)
            {
                Edges = (from useCaseNode in useCaseNodes
                         join useCaseToClass in classesOfUseCasesList on useCaseNode.IdNode equals useCaseToClass.IdUseCase
                         join classNode in classNodes on useCaseToClass.IdClass equals classNode.IdNode
                         select new clsEdgeItem
                         {
                             NodeItem1 = useCaseNode,
                             NodeItem2 = classNode,
                             OItem_RelationType = localConfig.OItem_relationtype_contains
                         }).ToList();
            }
            

            Edges.AddRange(from srcClass in Nodes
                           join attribute in classAttributes on srcClass.IdNode equals attribute.IdClass
                           join dstClass in Nodes on attribute.IdReferenceClass equals dstClass.IdNode
                           select new clsEdgeItem
                           {
                               NodeItem1 = srcClass,
                               NodeItem2 = dstClass,
                               OItem_RelationType = localConfig.Globals.RelationType_belongsTo
                           });
            return localConfig.Globals.LState_Success.Clone();
        }

        public List<clsGroupItem> GetGroupItemsOfNodes(List<clsGroupItem> groupItems, List<ClassesOfUseCases> classesOfUseCases, List<clsNodeItem> nodes)
        {
            var result = new List<clsGroupItem>();
            groupItems.ForEach(gi =>
                {
                    if (gi.Nodes != null && gi.Nodes.Any())
                    {
                        var nodeItems = (from nd in gi.Nodes
                                join classOfUseCase in classesOfUseCases on nd.IdNode equals classOfUseCase.IdUseCase
                                     join classNode in nodes on classOfUseCase.IdClass equals classNode.IdNode
                                     select classNode).GroupBy(cln => cln).Select(clng => clng.Key).ToList<clsNodeItem>();

                        if (nodeItems.Any())
                        {
                            gi.Nodes = nodeItems;
                            gi.Edges = new List<clsEdgeItem>();
                            result.Add(gi);
                        }
                        
                    }

                    result.AddRange(GetGroupItemsOfNodes(gi.GroupItems, classesOfUseCases, nodes));
                });

            return result;
            
        }
    }
}
