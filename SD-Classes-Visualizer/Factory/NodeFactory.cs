﻿using OntologyClasses.BaseClasses;
using SD_Classes_Visualizer.Data;
using SD_Classes_Visualizer.SDClassTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using UseCase_Visualizer;

namespace SD_Classes_Visualizer.Factory
{
    public class NodeFactory
    {
        public Image Image_Root { get; private set; }
        public Image Image_Project { get; private set; }
        public Image Image_SoftwareProject { get; private set; }
        public Image Image_UseCase { get; private set; }
        public Image Image_Classes { get; private set; }

        private clsLocalConfig localConfig;

        private List<clsObjectRel> projectHierarchy;
        private List<clsObjectRel> projectResources;
        private List<clsObjectRel> useCaseOfSoftwareProjects;

        public List<SDClassTreeNode> RootNodes { get; set; }
        public List<SDClassTreeNode> ProjectNodes { get; set; }

        private SDClassTreeNode rootNode;

        public List<SDClassTreeNode> GetNodes(List<clsObjectRel> projectHierarchy, 
            List<clsObjectRel> projectResources, 
            List<ProjectToSoftwareProject> softwareProjectToProject, 
            List<clsObjectRel> useCaseOfSoftwareProjects,
            List<ClassesOfUseCases> classesOfuseCases)
        {
            this.projectHierarchy = projectHierarchy;
            this.projectResources = projectResources;
            this.useCaseOfSoftwareProjects = useCaseOfSoftwareProjects;

            rootNode = new SDClassTreeNode
                {
                    IdNode = localConfig.Globals.Root.GUID,
                    NameNode = localConfig.Globals.Root.Name,
                    NodePath = localConfig.Globals.Root.GUID,
                    NodeType = NodeType.Root,
                    NodeImage = Image_Root
                };

            RootNodes = new List<SDClassTreeNode>();
            RootNodes.Add(rootNode);
            
            GetProjectNodes();

            ProjectNodes.ForEach(prjn =>
                {
                    var softwareProjectNodes = softwareProjectToProject.Where(sp => sp.IdProject == prjn.IdNode).Select(sp => new SDClassTreeNode
                        {
                            IdNode = sp.IdSoftwareProject,
                            NameNode = sp.NameSoftwareProject,
                            NodeImage = Image_SoftwareProject,
                            NodeType = NodeType.SoftwareProject,
                            NodePath = prjn.NodePath + sp.IdSoftwareProject
                        }).ToList();
                    prjn.Nodes.AddRange(softwareProjectNodes);

                    softwareProjectNodes.ForEach(sprj =>
                        {
                            var useCases = useCaseOfSoftwareProjects.Where(uc => uc.ID_Object == sprj.IdNode).Select(uc => new SDClassTreeNode
                            {
                                IdNode = uc.ID_Other,
                                NameNode = uc.Name_Other,
                                NodePath = prjn.NodePath + uc.ID_Other,
                                NodeType = NodeType.UseCase,
                                NodeImage = Image_UseCase
                            }).ToList(); ;
                            sprj.Nodes.AddRange(useCases);
                            useCases.ForEach(uc =>
                                {
                                    var classes = classesOfuseCases.Where(cls => cls.PathUseCase.Contains(@"\"  + uc.IdNode + @"\")).Select(cls => new SDClassTreeNode
                                        {
                                            IdNode = cls.IdClass,
                                            NameNode = cls.NameClass,
                                            NodePath = uc.IdNode + cls.IdClass,
                                            NodeType = NodeType.Class,
                                            NodeImage = Image_Classes
                                        }).ToList();

                                    uc.Nodes.AddRange(classes);
                                });

                            
                        });
                    
                    

                });

            return RootNodes;
        }

        private List<SDClassTreeNode> GetProjectNodes()
        {

            var projects = projectHierarchy.GroupBy(projh => new { GUID = projh.ID_Object, Name = projh.Name_Object, GUID_Parent = projh.ID_Parent_Object });

            rootNode.Nodes.AddRange(from project in projects
                         join parProject in projectHierarchy on project.Key.GUID equals parProject.ID_Other into parProjects
                         from parProject in parProjects.DefaultIfEmpty()
                         where parProject == null
                         select new SDClassTreeNode
                         {
                             IdNode = project.Key.GUID,
                             NameNode = project.Key.Name,
                             NodeImage = Image_Project,
                             NodeType = NodeType.Project,
                             NodePath = rootNode.IdNode + project.Key.GUID
                         });

            rootNode.Nodes.ForEach(node => GetSubNodes(node));

            return RootNodes;


        }

        private void GetSubNodes(SDClassTreeNode parentNode)
        {
            var subNodes = projectHierarchy.Where(prj => prj.ID_Object == parentNode.IdNode).Select(prj => new SDClassTreeNode()
            {
                IdNode = prj.ID_Other,
                NameNode = prj.Name_Other,
                NodeImage = Image_Project,
                NodeType = NodeType.Project,
                NodePath = parentNode.NodePath + prj.ID_Other
            }).ToList();
            parentNode.Nodes.AddRange(subNodes.ToArray());

            ProjectNodes.AddRange(from subNode in subNodes
                                  join projectResource in projectResources on subNode.IdNode equals projectResource.ID_Other
                                  select subNode);

            subNodes.ForEach(subNode => GetSubNodes(subNode));

        }

        public NodeFactory(clsLocalConfig localConfig )
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            Image_Project = new Image();
            Image_Project.Source = new BitmapImage { UriSource = new Uri(@"..\Resources\cahiers_de_labo.png", UriKind.Relative) };

            Image_Root = new Image();
            Image_Root.Source = new BitmapImage { UriSource = new Uri(@"..\Resources\bb_home_.png", UriKind.Relative) };

            Image_SoftwareProject = new Image();
            Image_SoftwareProject.Source = new BitmapImage { UriSource = new Uri(@"..\Resources\Procedures.png", UriKind.Relative) };

            Image_UseCase = new Image();
            Image_UseCase.Source = new BitmapImage { UriSource = new Uri(@"..\Resources\edit-find-replace.png", UriKind.Relative) };

            Image_Classes = new Image();
            Image_Classes.Source = new BitmapImage { UriSource = new Uri(@"..\Resources\XSDSchema_SequenceIcon.png", UriKind.Relative) };

            
            ProjectNodes = new List<SDClassTreeNode>();
            
        }
    }
}
