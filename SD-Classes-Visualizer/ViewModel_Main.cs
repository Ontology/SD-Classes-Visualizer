﻿using OntologyAppDBConnector;
using OntoMsg_Module;
using SD_Classes_Visualizer.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SD_Classes_Visualizer
{
    public class ViewModel_Main : ViewModelBase
    {
        private clsLocalConfig localConfig;

        private string closeCaption;
        public string CloseCaption
        {
            get { return closeCaption; }
            set
            {
                closeCaption = value;
                RaisePropertyChanged("CloseCaption");
            }
        }

        private string windowCaption;
        public string WindowCaption
        {
            get { return windowCaption; }
            set
            {
                windowCaption = value;
                RaisePropertyChanged("WindowCaption");
            }
        }

        public Globals Globals
        {
            get { return localConfig.Globals; }
            set
            {
                localConfig.Globals = value;
            }
        }

        public void Initialize()
        {
            SetCaption();
            
        }

        private void SetCaption()
        {
            CloseCaption = "Close";
            WindowCaption = "SD-Class-Visualizer";
        }

       
        public ViewModel_Main()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }
            RaisePropertyChanged("Globals");

            Initialize();
        }
    }
}
